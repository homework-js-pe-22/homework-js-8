document.addEventListener("DOMContentLoaded", function(evt) {
    console.log("DOM fully loaded and parsed");
})

let input = document.createElement('input');
input.setAttribute('value', 'Price');
input.addEventListener("focus", function() {
    input.style.cssText = "outline:none; border:1px solid green";
})
input.addEventListener('blur', function() {
    let val = input.value;

    input.style.cssText = "border:1px solid green";
    const span = document.createElement('span');
    span.style.cssText = "display:block";
    span.innerText = `Текущая цена: ${val}`;


    const button = document.createElement('button');
    button.innerHTML = 'X';

    span.append(button);
    input.style.cssText = "color:green";


    if (val <= 0) {
        error = true;
        input.style.cssText = "border:1px solid red";
        const p = document.createElement('p');
        p.innerHTML = 'Please enter correct price';
        input.after(p);
        return;
    }

    input.before(span)
})

document.body.onclick = function(evt) {
    // console.log(evt.target.tagName);
    if (evt.target.tagName == 'BUTTON') {
        evt.target.closest('span').remove();
        input.value = 'Price';
    }
}

document.body.append(input);